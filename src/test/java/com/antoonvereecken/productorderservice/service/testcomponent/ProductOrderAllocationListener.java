package com.antoonvereecken.productorderservice.service.testcomponent;

import com.antoonvereecken.common.event.AllocateOrderRequest;
import com.antoonvereecken.common.event.AllocateOrderResult;
import com.antoonvereecken.productorderservice.config.JmsConfig;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component
public class ProductOrderAllocationListener {

    public static final String FAIL_ALLOCATION_CUSTOMER_REF = "fail-allocation";
    public static final String PARTIAL_ALLOCATION_CUSTOMER_REF = "partial-allocation";
    public static final String DO_NOT_ALLOCATE_CUSTOMER_REF = "do-not-allocate";

    private final JmsTemplate jmsTemplate;

    @JmsListener(destination = JmsConfig.ALLOCATE_ORDER_QUEUE)
    public void listen(Message<?> msg) {
        AllocateOrderRequest request = (AllocateOrderRequest) msg.getPayload();
        boolean hasPendingInventory = false;
        boolean hasAllocationError = false;
        boolean sendResponse = true;

        if(request.getProductOrderDto().getCustomerRef() != null) {
            switch (request.getProductOrderDto().getCustomerRef()) {
                case PARTIAL_ALLOCATION_CUSTOMER_REF:
                    hasPendingInventory = true;
                    break;
                case FAIL_ALLOCATION_CUSTOMER_REF:
                    hasAllocationError = true;
                    break;
                case DO_NOT_ALLOCATE_CUSTOMER_REF:
                    sendResponse = false;
                    break;
            }
        }

        boolean finalHasPendingInventory = hasPendingInventory;
        request.getProductOrderDto().getProductOrderLines().forEach(line -> {
            if (finalHasPendingInventory) {
                line.setAllocatedQuantity(line.getOrderQuantity() - 1);
            } else {
                line.setAllocatedQuantity(line.getOrderQuantity());
            }
        });

        if (sendResponse) {
            jmsTemplate.convertAndSend(
                    JmsConfig.ALLOCATE_ORDER_RESULT_QUEUE,
                    AllocateOrderResult.builder()
                            .productOrderDto(request.getProductOrderDto())
                            .hasPendingInventory(hasPendingInventory)
                            .hasAllocationError(hasAllocationError)
                            .build()
            );
        }
    }


}
