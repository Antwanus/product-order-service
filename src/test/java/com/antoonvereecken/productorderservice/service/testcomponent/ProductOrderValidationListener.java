package com.antoonvereecken.productorderservice.service.testcomponent;

import com.antoonvereecken.common.event.ValidateOrderRequest;
import com.antoonvereecken.common.event.ValidateOrderResult;
import com.antoonvereecken.productorderservice.config.JmsConfig;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component
public class ProductOrderValidationListener {

    public static final String FAIL_VALIDATION_CUSTOMER_REF = "fail-validation";
    public static final String DO_NOT_VALIDATE_CUSTOMER_REF = "do-not-validate";

    private final JmsTemplate jmsTemplate;

    @JmsListener(destination = JmsConfig.VALIDATE_ORDER_QUEUE)
    public void listen(Message<?> msg) {
        boolean orderIsValid = true;
        boolean sendResponse = true;

        ValidateOrderRequest request = (ValidateOrderRequest) msg.getPayload();

        if (request.getOrderDto().getCustomerRef() != null) {
            switch (request.getOrderDto().getCustomerRef()) {
                case FAIL_VALIDATION_CUSTOMER_REF:
                    orderIsValid = false;
                    break;
                case DO_NOT_VALIDATE_CUSTOMER_REF:
                    sendResponse = false;
                    break;
            }
        }

        if (sendResponse) {
            jmsTemplate.convertAndSend(
                    JmsConfig.VALIDATE_ORDER_RESULT_QUEUE,
                    ValidateOrderResult.builder()
                            .orderId(request.getOrderDto().getId())
                            .isValid(orderIsValid)
                            .build());
        }
    }

}
