package com.antoonvereecken.productorderservice.service;

import com.antoonvereecken.common.event.AllocationFailure;
import com.antoonvereecken.common.event.DeallocateOrderRequest;
import com.antoonvereecken.common.model.ProductDto;
import com.antoonvereecken.productorderservice.config.JmsConfig;
import com.antoonvereecken.productorderservice.domain.Customer;
import com.antoonvereecken.productorderservice.domain.ProductOrder;
import com.antoonvereecken.productorderservice.domain.ProductOrderLine;
import com.antoonvereecken.productorderservice.domain.ProductOrderStatusEnum;
import com.antoonvereecken.productorderservice.repository.CustomerRepo;
import com.antoonvereecken.productorderservice.repository.ProductOrderRepo;
import com.antoonvereecken.productorderservice.service.order.ProductOrderManager;
import com.antoonvereecken.productorderservice.service.product.ProductServiceImpl;
import com.antoonvereecken.productorderservice.service.testcomponent.ProductOrderAllocationListener;
import com.antoonvereecken.productorderservice.service.testcomponent.ProductOrderValidationListener;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.jenspiegsa.wiremockextension.WireMockExtension;
import com.github.tomakehurst.wiremock.WireMockServer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.core.JmsTemplate;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import static com.github.jenspiegsa.wiremockextension.ManagedWireMockServer.with;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.okJson;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(WireMockExtension.class)
@SpringBootTest
class ProductOrderManagerImplIT {

    @Autowired ProductOrderManager productOrderManager;
    @Autowired ProductOrderRepo orderRepo;
    @Autowired CustomerRepo customerRepo;
    @Autowired ObjectMapper objectMapper;
    @Autowired WireMockServer wireMockServer;
    @Autowired JmsTemplate jmsTemplate;

    Customer testCustomer;
    ProductDto testProductDto;
    ProductOrder testOrder;
    UUID productId = UUID.randomUUID();
    String eanId = "123456789012";

    @TestConfiguration
    static class RestTemplateBuilderProvider {
        @Bean(destroyMethod = "stop") // shuts bean down when context stops
        public WireMockServer wireMockServer() {
            WireMockServer server = with(wireMockConfig().port(8083));
            server.start();

            return server;
        }
    }
    @BeforeEach
    void firstCreateTestCustomer() {
        testCustomer = customerRepo.save(Customer.builder()
                .customerName("Test Customer")
                .build());
    }
    @BeforeEach
    void firstCreateTestProductDto() {
        testProductDto = ProductDto.builder()
                .id(productId)
                .eanId(eanId)
                .build();
    }
    @BeforeEach
    void firstCreateTestOrderWithTestCustomerAndTestProduct() {
        testOrder = ProductOrder.builder()
                .customer(testCustomer)
                .build();

        Set<ProductOrderLine> orderLines = new HashSet<>();
        orderLines.add(ProductOrderLine.builder()
                .productId(testProductDto.getId())
                .eanId(testProductDto.getEanId())
                .orderQuantity(1)
                .productOrder(testOrder)
                .build());

        testOrder.setProductOrderLines(orderLines);

    }

    @Test
    void testNewToAllocated() throws JsonProcessingException {
        wireMockServer.stubFor(
                get(ProductServiceImpl.PRODUCT_EAN_PATH_V1 + testProductDto.getEanId())
                    .willReturn(okJson(objectMapper.writeValueAsString(testProductDto)))
        );
        ProductOrder savedProductOrder = productOrderManager.newProductOrder(testOrder);

        await().untilAsserted( () -> {
            ProductOrder foundOrder = orderRepo.findById(testOrder.getId()).get();
            assertEquals(ProductOrderStatusEnum.ALLOCATED, foundOrder.getProductOrderStatus());
        });

        await().untilAsserted( () -> {
            ProductOrder foundOrder = orderRepo.findById(testOrder.getId()).get();

            ProductOrderLine line = foundOrder.getProductOrderLines().iterator().next();
            assertEquals(line.getOrderQuantity(), line.getAllocatedQuantity());
        });

        ProductOrder savedProductOrder2 = orderRepo.findById(savedProductOrder.getId()).get();

        assertNotNull(savedProductOrder);
        assertEquals(ProductOrderStatusEnum.ALLOCATED, savedProductOrder2.getProductOrderStatus());
        savedProductOrder2.getProductOrderLines().forEach(line ->
            assertEquals(line.getOrderQuantity(), line.getAllocatedQuantity())
        );
    }

    @Test
    void testNewToDelivered() throws JsonProcessingException {
        ProductDto productDto = ProductDto.builder()
                .id(productId)
                .eanId(eanId)
                .build();

        wireMockServer.stubFor(
                get(ProductServiceImpl.PRODUCT_EAN_PATH_V1 + eanId)
                        .willReturn(okJson(objectMapper.writeValueAsString(productDto)))
        );

        ProductOrder productOrder = createProductOrder();
        ProductOrder savedProductOrder = productOrderManager.newProductOrder(productOrder);

        await().untilAsserted( () -> {
            ProductOrder foundOrder = orderRepo.findById(productOrder.getId()).get();
            assertEquals(ProductOrderStatusEnum.ALLOCATED, foundOrder.getProductOrderStatus());
        });

        productOrderManager.productOrderDelivered(savedProductOrder.getId());

        await().untilAsserted( () -> {
            ProductOrder foundOrder = orderRepo.findById(productOrder.getId()).get();
            assertEquals(ProductOrderStatusEnum.DELIVERED, foundOrder.getProductOrderStatus());
        });

        ProductOrder deliveredOrder = orderRepo.findById(savedProductOrder.getId()).get();

        assertEquals(ProductOrderStatusEnum.DELIVERED, deliveredOrder.getProductOrderStatus());

    }

    @Test
    void testValidationFailed() throws JsonProcessingException {
        ProductDto productDto = ProductDto.builder()
                .id(productId)
                .eanId(eanId)
                .build();

        wireMockServer.stubFor(
                get(ProductServiceImpl.PRODUCT_EAN_PATH_V1 + eanId)
                        .willReturn(okJson(objectMapper.writeValueAsString(productDto)))
        );

        ProductOrder productOrder = createProductOrder();
        productOrder.setCustomerRef(ProductOrderValidationListener.FAIL_VALIDATION_CUSTOMER_REF);
        productOrderManager.newProductOrder(productOrder);

        await().untilAsserted( () -> {
            ProductOrder foundOrder = orderRepo.findById(productOrder.getId()).get();
            assertEquals(ProductOrderStatusEnum.VALIDATION_FAILED, foundOrder.getProductOrderStatus());
        });
    }

    @Test
    void testAllocationFailure() throws JsonProcessingException {
        ProductDto productDto = ProductDto.builder()
                .id(productId)
                .eanId(eanId)
                .build();

        wireMockServer.stubFor(get(ProductServiceImpl.PRODUCT_EAN_PATH_V1 + eanId)
                        .willReturn(okJson(objectMapper.writeValueAsString(productDto)))
        );

        ProductOrder productOrder = createProductOrder();
        productOrder.setCustomerRef(ProductOrderAllocationListener.FAIL_ALLOCATION_CUSTOMER_REF);

        ProductOrder savedOrder = productOrderManager.newProductOrder(productOrder);

        await().untilAsserted( () -> {
            ProductOrder foundOrder = orderRepo.findById(productOrder.getId()).get();
            assertEquals(ProductOrderStatusEnum.ALLOCATION_FAILED, foundOrder.getProductOrderStatus());
        });

        AllocationFailure event = (AllocationFailure) jmsTemplate.receiveAndConvert(JmsConfig.ALLOCATION_FAILURE_QUEUE);

        assertNotNull(event);

        assertEquals(event.getOrderId(), savedOrder.getId());
    }

    @Test
    void testPartialAllocation() throws JsonProcessingException {
        ProductDto productDto = ProductDto.builder()
            .id(productId)
            .eanId(eanId)
            .build();

        wireMockServer.stubFor(
                get(ProductServiceImpl.PRODUCT_EAN_PATH_V1 + eanId)
                        .willReturn(okJson(objectMapper.writeValueAsString(productDto)))
        );

        ProductOrder productOrder = createProductOrder();
        productOrder.setCustomerRef(ProductOrderAllocationListener.PARTIAL_ALLOCATION_CUSTOMER_REF);

        productOrderManager.newProductOrder(productOrder);

        await().untilAsserted( () -> {
            ProductOrder foundOrder = orderRepo.findById(productOrder.getId()).get();
            assertEquals(ProductOrderStatusEnum.ALLOCATION_PENDING, foundOrder.getProductOrderStatus());
        });

    }

    @Test
    void testValidationPendingToCancel() throws JsonProcessingException {
        ProductDto productDto = ProductDto.builder()
                .id(productId)
                .eanId(eanId)
                .build();

        wireMockServer.stubFor(get(ProductServiceImpl.PRODUCT_EAN_PATH_V1 + eanId)
                .willReturn(okJson(objectMapper.writeValueAsString(productDto))));

        ProductOrder productOrder = createProductOrder();
        productOrder.setCustomerRef(ProductOrderValidationListener.DO_NOT_VALIDATE_CUSTOMER_REF);

        ProductOrder savedOrder = productOrderManager.newProductOrder(productOrder);

        await().untilAsserted( () -> {
            ProductOrder foundOrder = orderRepo.findById(productOrder.getId()).get();
            assertEquals(ProductOrderStatusEnum.VALIDATION_PENDING, foundOrder.getProductOrderStatus());
        });

        productOrderManager.cancelOrder(savedOrder.getId());

        await().untilAsserted( () -> {
            ProductOrder foundOrder = orderRepo.findById(productOrder.getId()).get();
            assertEquals(ProductOrderStatusEnum.CANCELED, foundOrder.getProductOrderStatus());
        });
    }

    @Test
    void testAllocationPendingToCancel() throws JsonProcessingException {
        ProductDto productDto = ProductDto.builder()
                .id(productId)
                .eanId(eanId)
                .build();

        wireMockServer.stubFor(get(ProductServiceImpl.PRODUCT_EAN_PATH_V1 + eanId)
                .willReturn(okJson(objectMapper.writeValueAsString(productDto))));

        ProductOrder productOrder = createProductOrder();
        productOrder.setCustomerRef(ProductOrderAllocationListener.DO_NOT_ALLOCATE_CUSTOMER_REF);

        ProductOrder savedOrder = productOrderManager.newProductOrder(productOrder);

        await().untilAsserted( () -> {
            ProductOrder foundOrder = orderRepo.findById(productOrder.getId()).get();
            assertEquals(ProductOrderStatusEnum.ALLOCATION_PENDING, foundOrder.getProductOrderStatus());
        });

        productOrderManager.cancelOrder(savedOrder.getId());

        await().untilAsserted( () -> {
            ProductOrder foundOrder = orderRepo.findById(productOrder.getId()).get();
            assertEquals(ProductOrderStatusEnum.CANCELED, foundOrder.getProductOrderStatus());
        });
    }
    @Test
    void testAllocatedToCancel() throws JsonProcessingException {
        ProductDto productDto = ProductDto.builder()
                .id(productId)
                .eanId(eanId)
                .build();

        wireMockServer.stubFor(get(ProductServiceImpl.PRODUCT_EAN_PATH_V1 + eanId)
                .willReturn(okJson(objectMapper.writeValueAsString(productDto))));

        ProductOrder productOrder = createProductOrder();

        ProductOrder savedOrder = productOrderManager.newProductOrder(productOrder);

        await().untilAsserted( () -> {
            ProductOrder foundOrder = orderRepo.findById(productOrder.getId()).get();
            assertEquals(ProductOrderStatusEnum.ALLOCATED, foundOrder.getProductOrderStatus());
        });

        productOrderManager.cancelOrder(savedOrder.getId());

        await().untilAsserted( () -> {
            ProductOrder foundOrder = orderRepo.findById(productOrder.getId()).get();
            assertEquals(ProductOrderStatusEnum.CANCELED, foundOrder.getProductOrderStatus());
        });

        DeallocateOrderRequest deallocateOrderRequest = (DeallocateOrderRequest) jmsTemplate.receiveAndConvert(JmsConfig.DEALLOCATE_ORDER_QUEUE);

        assertNotNull(deallocateOrderRequest);
        assertEquals(deallocateOrderRequest.getProductOrderDto().getId(), savedOrder.getId());
    }



    public ProductOrder createProductOrder() {
        ProductOrder productOrder = ProductOrder.builder()
                .customer(testCustomer)
                .build();

        Set<ProductOrderLine> orderLines = new HashSet<>();
        orderLines.add(ProductOrderLine.builder()
                .productId(productId)
                .eanId(eanId)
                .orderQuantity(1)
                .productOrder(productOrder)
                .build());

        productOrder.setProductOrderLines(orderLines);

        return productOrder;
    }


}
