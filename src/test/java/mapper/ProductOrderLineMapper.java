package mapper;

import com.antoonvereecken.common.model.ProductOrderLineDto;
import com.antoonvereecken.productorderservice.domain.ProductOrderLine;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;

@DecoratedWith(ProductOrderLineMapperDecorator.class)
@Mapper(uses = { DateMapper.class })
public interface ProductOrderLineMapper {

     ProductOrderLineDto productOrderLineToDto(ProductOrderLine productOrderLine);

     ProductOrderLine dtoToProductOrderLine(ProductOrderLineDto dto); 

}
