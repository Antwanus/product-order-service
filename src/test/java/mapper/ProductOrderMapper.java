package mapper;

import com.antoonvereecken.common.model.ProductOrderDto;
import com.antoonvereecken.productorderservice.domain.ProductOrder;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(uses = { DateMapper.class, ProductOrderLineMapper.class })
public interface ProductOrderMapper {

    @Mapping(target = "customerId", source = "customer.id")
    ProductOrderDto productOrderToDto(ProductOrder productOrder);

    ProductOrder dtoToProductOrder(ProductOrderDto dto);


}
