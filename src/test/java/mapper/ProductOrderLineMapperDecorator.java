package mapper;

import com.antoonvereecken.common.model.ProductDto;
import com.antoonvereecken.common.model.ProductOrderLineDto;
import com.antoonvereecken.productorderservice.domain.ProductOrderLine;
import com.antoonvereecken.productorderservice.service.product.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.Optional;

public abstract class ProductOrderLineMapperDecorator implements ProductOrderLineMapper {

    private ProductService productService;
    private ProductOrderLineMapper mapper;

    @Autowired
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }
    @Autowired
    @Qualifier("delegate")
    public void setMapper(ProductOrderLineMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public ProductOrderLineDto productOrderLineToDto(ProductOrderLine orderLine) {
        ProductOrderLineDto dto = mapper.productOrderLineToDto(orderLine);
        Optional<ProductDto> optionalDto = productService.getProductByEanId(orderLine.getEanId());

        optionalDto.ifPresent(productDto -> {
            dto.setId(productDto.getId());
            dto.setEanId(productDto.getEanId());
            dto.setProductName(productDto.getProductName());
            dto.setProductCategory(productDto.getProductCategory());
            dto.setPrice(productDto.getProductPrice());
        });

        return dto;
    }

}
