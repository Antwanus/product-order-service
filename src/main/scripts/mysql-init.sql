DROP DATABASE IF EXISTS productorderservice;
DROP USER IF EXISTS `product_order_service`@`%`;
CREATE DATABASE IF NOT EXISTS productorderservice CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
CREATE USER IF NOT EXISTS `product_order_service`@`%` IDENTIFIED WITH mysql_native_password BY 'password';
GRANT SELECT, INSERT, UPDATE, DELETE, CREATE, DROP, REFERENCES, INDEX, ALTER, EXECUTE, CREATE VIEW, SHOW VIEW,
CREATE ROUTINE, ALTER ROUTINE, EVENT, TRIGGER ON `productorderservice`.* TO `product_order_service`@`%`;
FLUSH PRIVILEGES;