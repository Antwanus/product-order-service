package com.antoonvereecken.productorderservice.bootstrap;

import com.antoonvereecken.productorderservice.domain.Customer;
import com.antoonvereecken.productorderservice.repository.CustomerRepo;
import com.antoonvereecken.productorderservice.repository.ProductOrderRepo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Slf4j
@RequiredArgsConstructor
@Component
public class ProductOrderBootstrap implements CommandLineRunner {

    public static final String CHAOS_MONKEY = "Chaos Monkey";
    public static final String PRODUCT_EAN_1 = "1111111111111";
    public static final String PRODUCT_EAN_2 = "2222222222222";
    public static final String PRODUCT_EAN_3 = "3333333333333";

    private final CustomerRepo customerRepo;
    private final ProductOrderRepo productOrderRepo;

    @Override
    public void run(String... args) {
        loadChaosMonkeyToCustomers();
    }
        private void loadChaosMonkeyToCustomers() {
            if (customerRepo.findAllCustomersByCustomerNameLike(CHAOS_MONKEY).isEmpty()) {
                Customer c = customerRepo.save(
                        Customer.builder()
                            .customerName(CHAOS_MONKEY)
                            .apiKey(UUID.randomUUID())
                            .build()
                );
                log.debug("Customer: " + c.getCustomerName() + ", ID: " + c.getId());
            }
        }


}
