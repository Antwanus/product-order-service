package com.antoonvereecken.productorderservice.repository;

import com.antoonvereecken.productorderservice.domain.Customer;
import com.antoonvereecken.productorderservice.domain.ProductOrder;
import com.antoonvereecken.productorderservice.domain.ProductOrderStatusEnum;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;

import javax.persistence.LockModeType;
import java.util.List;
import java.util.UUID;

public interface ProductOrderRepo extends JpaRepository<ProductOrder, UUID> {

    Page<ProductOrder> findAllByCustomer(Customer customer, Pageable pageable);

    List<ProductOrder> findAllByProductOrderStatus(ProductOrderStatusEnum productOrderStatusEnum);

    /* this is returning null in IT testing... H2 problem?
    @Lock(LockModeType.PESSIMISTIC_WRITE) //obtain exclusive lock and prevent the data from being read, updated or deleted
    ProductOrder findOneById(UUID id);
     */
}
