package com.antoonvereecken.productorderservice.repository;

import com.antoonvereecken.productorderservice.domain.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface CustomerRepo extends JpaRepository<Customer, UUID> {

    List<Customer> findAllCustomersByCustomerNameLike(String customerName);

}
