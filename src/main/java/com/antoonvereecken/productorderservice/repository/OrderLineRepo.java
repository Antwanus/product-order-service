package com.antoonvereecken.productorderservice.repository;

import com.antoonvereecken.productorderservice.domain.ProductOrder;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface OrderLineRepo extends JpaRepository<ProductOrder, UUID> {

}
