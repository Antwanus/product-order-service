package com.antoonvereecken.productorderservice.statemachine;

import com.antoonvereecken.productorderservice.domain.ProductOrderEventEnum;
import com.antoonvereecken.productorderservice.domain.ProductOrderStatusEnum;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.action.Action;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.StateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import java.util.EnumSet;

@RequiredArgsConstructor
@Configuration
@EnableStateMachineFactory
public class ProductOrderStateMachineConfig
        extends StateMachineConfigurerAdapter<ProductOrderStatusEnum, ProductOrderEventEnum> {

    private final Action<ProductOrderStatusEnum, ProductOrderEventEnum> validateOrderAction;
    private final Action<ProductOrderStatusEnum, ProductOrderEventEnum> validationFailureAction;
    private final Action<ProductOrderStatusEnum, ProductOrderEventEnum> allocateOrderAction;
    private final Action<ProductOrderStatusEnum, ProductOrderEventEnum> allocationFailureAction;
    private final Action<ProductOrderStatusEnum, ProductOrderEventEnum> deallocateOrderAction;


    @Override
    public void configure(StateMachineStateConfigurer<ProductOrderStatusEnum, ProductOrderEventEnum> states) throws Exception {
        states.withStates()
                .initial(ProductOrderStatusEnum.NEW)
                .states(EnumSet.allOf(ProductOrderStatusEnum.class))
                .end(ProductOrderStatusEnum.DELIVERY_SUCCESS)
                .end(ProductOrderStatusEnum.DELIVERED)
                .end(ProductOrderStatusEnum.DELIVERY_FAILED)
                .end(ProductOrderStatusEnum.VALIDATION_FAILED)
                .end(ProductOrderStatusEnum.ALLOCATION_FAILED)
                .end(ProductOrderStatusEnum.CANCELED);
    }

    @Override
    public void configure(StateMachineTransitionConfigurer<ProductOrderStatusEnum, ProductOrderEventEnum> transitions) throws Exception {
        transitions
            .withExternal()
                .source(ProductOrderStatusEnum.NEW).target(ProductOrderStatusEnum.VALIDATION_PENDING)
                .event(ProductOrderEventEnum.VALIDATE_ORDER)
                .action(validateOrderAction)

            .and().withExternal()
                .source(ProductOrderStatusEnum.VALIDATION_PENDING).target(ProductOrderStatusEnum.VALIDATED)
                .event(ProductOrderEventEnum.VALIDATION_SUCCESS)
            .and().withExternal()
                .source(ProductOrderStatusEnum.VALIDATION_PENDING).target(ProductOrderStatusEnum.VALIDATION_FAILED)
                .event(ProductOrderEventEnum.VALIDATION_ERROR)
                .action(validationFailureAction)
            .and().withExternal()
                .source(ProductOrderStatusEnum.VALIDATION_PENDING).target(ProductOrderStatusEnum.CANCELED)
                .event(ProductOrderEventEnum.ORDER_CANCELED)

            .and().withExternal()
                .source(ProductOrderStatusEnum.VALIDATED).target(ProductOrderStatusEnum.ALLOCATION_PENDING)
                .event(ProductOrderEventEnum.ALLOCATE_ORDER)
                .action(allocateOrderAction)
            .and().withExternal()
                .source(ProductOrderStatusEnum.VALIDATED).target(ProductOrderStatusEnum.CANCELED)
                .event(ProductOrderEventEnum.ORDER_CANCELED)

            .and().withExternal()
                .source(ProductOrderStatusEnum.ALLOCATION_PENDING).target(ProductOrderStatusEnum.ALLOCATED)
                .event(ProductOrderEventEnum.ALLOCATION_SUCCESS)
            .and().withExternal()
                .source(ProductOrderStatusEnum.ALLOCATION_PENDING).target(ProductOrderStatusEnum.ALLOCATION_FAILED)
                .event(ProductOrderEventEnum.ALLOCATION_ERROR)
                .action(allocationFailureAction)
            .and().withExternal()
                .source(ProductOrderStatusEnum.ALLOCATION_PENDING).target(ProductOrderStatusEnum.ALLOCATION_PENDING)
                .event(ProductOrderEventEnum.ALLOCATION_NO_INVENTORY)
                .and().withExternal()
                .source(ProductOrderStatusEnum.ALLOCATION_PENDING).target(ProductOrderStatusEnum.CANCELED)
                .event(ProductOrderEventEnum.ORDER_CANCELED)

            .and().withExternal()
                .source(ProductOrderStatusEnum.ALLOCATED).target(ProductOrderStatusEnum.DELIVERED)
                .event(ProductOrderEventEnum.ORDER_COMPLETED)
            .and().withExternal()
                .source(ProductOrderStatusEnum.ALLOCATED).target(ProductOrderStatusEnum.CANCELED)
                .event(ProductOrderEventEnum.ORDER_CANCELED)
                .action(deallocateOrderAction);

    }


}
