package com.antoonvereecken.productorderservice.statemachine.action;

import com.antoonvereecken.common.event.AllocationFailure;
import com.antoonvereecken.productorderservice.config.JmsConfig;
import com.antoonvereecken.productorderservice.domain.ProductOrderEventEnum;
import com.antoonvereecken.productorderservice.domain.ProductOrderStatusEnum;
import com.antoonvereecken.productorderservice.service.order.ProductOrderManagerImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Slf4j
@RequiredArgsConstructor
@Component
public class AllocationFailureAction implements Action<ProductOrderStatusEnum, ProductOrderEventEnum> {

    private final JmsTemplate jmsTemplate;

    @Override
    public void execute(StateContext<ProductOrderStatusEnum, ProductOrderEventEnum> context) {

        String orderId = (String) context.getMessageHeaders().get(ProductOrderManagerImpl.ORDER_ID_HEADER);

        if (orderId != null) {
            jmsTemplate.convertAndSend(
                    JmsConfig.ALLOCATION_FAILURE_QUEUE,
                    AllocationFailure.builder()
                            .orderId(UUID.fromString(orderId))
                            .build()
            );
            log.debug("ALLOCATION FAILURE SENT TO QUEUE - ORDER: " + orderId);
        } else {
            log.debug("ORDER_ID IS NULL");
        }
    }


}
