package com.antoonvereecken.productorderservice.statemachine.action;

import com.antoonvereecken.productorderservice.domain.ProductOrderEventEnum;
import com.antoonvereecken.productorderservice.domain.ProductOrderStatusEnum;
import com.antoonvereecken.productorderservice.service.order.ProductOrderManagerImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ValidationFailureAction implements Action<ProductOrderStatusEnum, ProductOrderEventEnum> {

    @Override
    public void execute(StateContext<ProductOrderStatusEnum, ProductOrderEventEnum> context) {
        String orderId = (String) context.getMessageHeaders().get(ProductOrderManagerImpl.ORDER_ID_HEADER);
        log.error("ValidationFailureAction>ORDER> " + orderId);
    }
}
