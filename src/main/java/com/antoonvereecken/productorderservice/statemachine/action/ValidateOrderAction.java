package com.antoonvereecken.productorderservice.statemachine.action;

import com.antoonvereecken.common.event.ValidateOrderRequest;
import com.antoonvereecken.productorderservice.config.JmsConfig;
import com.antoonvereecken.productorderservice.domain.ProductOrder;
import com.antoonvereecken.productorderservice.domain.ProductOrderEventEnum;
import com.antoonvereecken.productorderservice.domain.ProductOrderStatusEnum;
import com.antoonvereecken.productorderservice.repository.ProductOrderRepo;
import com.antoonvereecken.productorderservice.service.order.ProductOrderManagerImpl;
import com.antoonvereecken.productorderservice.web.mapper.ProductOrderMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.UUID;

@Slf4j
@RequiredArgsConstructor
@Component
public class ValidateOrderAction implements Action<ProductOrderStatusEnum, ProductOrderEventEnum> {

    private final ProductOrderRepo orderRepo;
    private final ProductOrderMapper orderMapper;
    private final JmsTemplate jmsTemplate;


    @Override
    public void execute(StateContext<ProductOrderStatusEnum, ProductOrderEventEnum> context) {
        String productOrderId = (String) context.getMessageHeaders().get(ProductOrderManagerImpl.ORDER_ID_HEADER);

        Optional<ProductOrder> productOrderOptional = orderRepo.findById(UUID.fromString(productOrderId));

        productOrderOptional.ifPresentOrElse(productOrder -> {

            jmsTemplate.convertAndSend(
                    JmsConfig.VALIDATE_ORDER_QUEUE,
                    ValidateOrderRequest.builder()
                        .orderDto(orderMapper.productOrderToDto(productOrder))
                        .build());

        }, () -> log.error("°°°°°°°°°°° ORDER" + productOrderId +" NOT FOUND °°°°°°°°°°°°°°°°°°"));

        log.debug("ValidateOrderAction has sent a ValidateOrderRequest to the the queue for order " + productOrderId);

    }


}
