package com.antoonvereecken.productorderservice.statemachine.action;

import com.antoonvereecken.common.event.AllocateOrderRequest;
import com.antoonvereecken.productorderservice.config.JmsConfig;
import com.antoonvereecken.productorderservice.domain.ProductOrder;
import com.antoonvereecken.productorderservice.domain.ProductOrderEventEnum;
import com.antoonvereecken.productorderservice.domain.ProductOrderStatusEnum;
import com.antoonvereecken.productorderservice.repository.ProductOrderRepo;
import com.antoonvereecken.productorderservice.service.order.ProductOrderManagerImpl;
import com.antoonvereecken.productorderservice.web.mapper.ProductOrderMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.UUID;

@Slf4j
@RequiredArgsConstructor
@Component
public class AllocateOrderAction implements Action<ProductOrderStatusEnum, ProductOrderEventEnum> {

    private final ProductOrderRepo orderRepo;
    private final ProductOrderMapper orderMapper;
    private final JmsTemplate jmsTemplate;

    @Override
    public void execute(StateContext<ProductOrderStatusEnum, ProductOrderEventEnum> context) {
        // get id from header
        String productOrderId = (String) context.getMessageHeaders().get(ProductOrderManagerImpl.ORDER_ID_HEADER);
        // find this product in DB
        Optional<ProductOrder> productOrderOptional = orderRepo.findById(UUID.fromString(productOrderId));

        productOrderOptional.ifPresentOrElse(productOrder -> {
            // send AllocateOrderRequest on queue
            jmsTemplate.convertAndSend(
                JmsConfig.ALLOCATE_ORDER_QUEUE,
                AllocateOrderRequest.builder()
                        .productOrderDto(orderMapper.productOrderToDto(productOrder))
                        .build());


            log.debug("AllocateOrderAction has sent an AllocateOrderRequest to the the queue for order " + productOrderId);

        }, () -> log.error("°°°°°°°°°°° ORDER" + productOrderId +" NOT FOUND °°°°°°°°°°°°°°°°°°"));


    }

}
