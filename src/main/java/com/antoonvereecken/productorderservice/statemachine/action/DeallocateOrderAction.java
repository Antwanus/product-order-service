package com.antoonvereecken.productorderservice.statemachine.action;

import com.antoonvereecken.common.event.AllocateOrderRequest;
import com.antoonvereecken.common.event.DeallocateOrderRequest;
import com.antoonvereecken.productorderservice.config.JmsConfig;
import com.antoonvereecken.productorderservice.domain.ProductOrder;
import com.antoonvereecken.productorderservice.domain.ProductOrderEventEnum;
import com.antoonvereecken.productorderservice.domain.ProductOrderStatusEnum;
import com.antoonvereecken.productorderservice.repository.ProductOrderRepo;
import com.antoonvereecken.productorderservice.service.order.ProductOrderManagerImpl;
import com.antoonvereecken.productorderservice.web.mapper.ProductOrderMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.UUID;

@Slf4j
@RequiredArgsConstructor
@Component
public class DeallocateOrderAction implements Action<ProductOrderStatusEnum, ProductOrderEventEnum> {

    private final ProductOrderRepo orderRepo;
    private final ProductOrderMapper orderMapper;
    private final JmsTemplate jmsTemplate;

    @Override
    public void execute(StateContext<ProductOrderStatusEnum, ProductOrderEventEnum> context) {
        String productOrderId = (String) context.getMessageHeaders().get(ProductOrderManagerImpl.ORDER_ID_HEADER);
        Optional<ProductOrder> productOrderOptional = orderRepo.findById(UUID.fromString(productOrderId));

        productOrderOptional.ifPresentOrElse(productOrder -> {
            jmsTemplate.convertAndSend(
                    JmsConfig.DEALLOCATE_ORDER_QUEUE,
                    DeallocateOrderRequest.builder()
                            .productOrderDto(orderMapper.productOrderToDto(productOrder))
                            .build());

            log.debug("DE-allocateOrderAction has sent an DeAllocateOrderRequest to the the queue for order " + productOrderId);

        }, () -> log.error("°°°°°°°°°°° ORDER" + productOrderId +" NOT FOUND °°°°°°°°°°°°°°°°°°"));


    }

}
