package com.antoonvereecken.productorderservice.statemachine.interceptor;

import com.antoonvereecken.productorderservice.domain.ProductOrder;
import com.antoonvereecken.productorderservice.domain.ProductOrderEventEnum;
import com.antoonvereecken.productorderservice.domain.ProductOrderStatusEnum;
import com.antoonvereecken.productorderservice.repository.ProductOrderRepo;
import com.antoonvereecken.productorderservice.service.order.ProductOrderManagerImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.Message;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.state.State;
import org.springframework.statemachine.support.StateMachineInterceptorAdapter;
import org.springframework.statemachine.transition.Transition;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

@Slf4j
@RequiredArgsConstructor
@Component
public class OrderStateChangeInterceptor extends StateMachineInterceptorAdapter<ProductOrderStatusEnum, ProductOrderEventEnum> {

    private final ProductOrderRepo orderRepo;

    /** PERSIST STATECHANGES TO THE DB
     *  - get the orderId from the Message.headers (see ProductOrderManagerImpl.sendProductOrderEvent())
     *  - get this order from DB
     *  - set the orderStatus to state.getId() (=> mapped by ProductOrderStatusEnum)
     *  - save & flush ("eager" save)
     * */
    @Transactional
    @Override
    public void preStateChange(
            State<ProductOrderStatusEnum, ProductOrderEventEnum> state,
            Message<ProductOrderEventEnum> message,
            Transition<ProductOrderStatusEnum, ProductOrderEventEnum> transition,
            StateMachine<ProductOrderStatusEnum, ProductOrderEventEnum> stateMachine
    ) {
        log.debug("PERSISTING STATE-CHANGE TO THE DB...");

        Optional.ofNullable(message)
                .flatMap(msg -> Optional.ofNullable((String) msg.getHeaders()
                        .getOrDefault(ProductOrderManagerImpl.ORDER_ID_HEADER, " ")))
                .ifPresent(orderId -> {
                    log.debug("Saving state for ORDER: " + orderId + ", STATUS: " + state.getId());
                    log.debug(state.toString());

                    ProductOrder order = orderRepo.getOne(UUID.fromString(orderId));
                    order.setProductOrderStatus(state.getId());
                    orderRepo.saveAndFlush(order);
                });
    }
}
