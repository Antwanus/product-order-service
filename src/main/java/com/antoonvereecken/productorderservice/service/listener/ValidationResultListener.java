package com.antoonvereecken.productorderservice.service.listener;

import com.antoonvereecken.common.event.ValidateOrderResult;
import com.antoonvereecken.productorderservice.config.JmsConfig;
import com.antoonvereecken.productorderservice.service.order.ProductOrderManager;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

@Slf4j
@RequiredArgsConstructor
@Service
public class ValidationResultListener {

    private final ProductOrderManager orderManager;

    @JmsListener(destination = JmsConfig.VALIDATE_ORDER_RESULT_QUEUE)
    public void listen(ValidateOrderResult result){
        log.debug("VALIDATION RESULT DETECTED FOR ORDERID: " + result.getOrderId());
        orderManager.processValidationResult(result.getOrderId(), result.getIsValid());
    }


}
