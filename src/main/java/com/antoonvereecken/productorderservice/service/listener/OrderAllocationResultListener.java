package com.antoonvereecken.productorderservice.service.listener;

import com.antoonvereecken.common.event.AllocateOrderResult;
import com.antoonvereecken.productorderservice.config.JmsConfig;
import com.antoonvereecken.productorderservice.service.order.ProductOrderManager;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component
public class OrderAllocationResultListener {

    private final ProductOrderManager productOrderManager;

    @JmsListener(destination = JmsConfig.ALLOCATE_ORDER_RESULT_QUEUE)
    public void listen(AllocateOrderResult allocateOrderResult) {
        Boolean hasError = allocateOrderResult.getHasAllocationError();
        Boolean hasPendingInventory = allocateOrderResult.getHasPendingInventory();

        if (Boolean.FALSE.equals(hasError) && Boolean.FALSE.equals(hasPendingInventory)) {
            productOrderManager.productOrderAllocationSuccess(allocateOrderResult.getProductOrderDto());
        } else if (Boolean.FALSE.equals(hasError) && Boolean.TRUE.equals(hasPendingInventory)) {
            productOrderManager.productOrderAllocationPendingInventory(allocateOrderResult.getProductOrderDto());
        } else if (Boolean.TRUE.equals(hasError)) {
            productOrderManager.productOrderAllocationFailed(allocateOrderResult.getProductOrderDto());
        }
    }

}
