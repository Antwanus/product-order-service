package com.antoonvereecken.productorderservice.service.product;

import com.antoonvereecken.common.model.ProductDto;

import java.util.Optional;
import java.util.UUID;

public interface ProductService {

    Optional<ProductDto> getProductById(UUID id);

    Optional<ProductDto> getProductByEanId(String eanId);

}
