package com.antoonvereecken.productorderservice.service.product;

import com.antoonvereecken.common.model.ProductDto;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Optional;
import java.util.UUID;

@ConfigurationProperties(prefix = "com.antoonvereecken", ignoreUnknownFields = false)
@Service
public class ProductServiceImpl implements ProductService {

    public static final String PRODUCT_PATH_V1 = "/api/v1/product/";
    public static final String PRODUCT_EAN_PATH_V1 = "/api/v1/product/ean/";
    private String productServiceHost;

    private final RestTemplate restTemplate;


    public ProductServiceImpl(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    @Override
    public Optional<ProductDto> getProductById(UUID id) {
        return Optional.ofNullable(
                restTemplate.getForObject(
                        productServiceHost + PRODUCT_PATH_V1 + id,
                        ProductDto.class
        ));
    }

    @Override
    public Optional<ProductDto> getProductByEanId(String eanId) {
        return Optional.ofNullable(
                restTemplate.getForObject(
                        productServiceHost + PRODUCT_EAN_PATH_V1 + eanId,
                        ProductDto.class
        ));
    }

    public void setProductServiceHost(String productServiceHost) {
        this.productServiceHost = productServiceHost;
    }


}
