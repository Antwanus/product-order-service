package com.antoonvereecken.productorderservice.service.chaosmonkey;

import com.antoonvereecken.common.model.ProductOrderDto;
import com.antoonvereecken.common.model.ProductOrderLineDto;
import com.antoonvereecken.productorderservice.bootstrap.ProductOrderBootstrap;
import com.antoonvereecken.productorderservice.domain.Customer;
import com.antoonvereecken.productorderservice.repository.CustomerRepo;
import com.antoonvereecken.productorderservice.repository.ProductOrderRepo;
import com.antoonvereecken.productorderservice.service.order.ProductOrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

@Slf4j
@Service
public class OrderMonkeyService {

    private final CustomerRepo customerRepo;
    private final ProductOrderRepo productOrderRepo;
    private final ProductOrderService orderService;

    private final List<String> productEanIdList = new ArrayList<>(3);

    public OrderMonkeyService(CustomerRepo customerRepo, ProductOrderRepo productOrderRepo, ProductOrderService orderService) {
        this.customerRepo = customerRepo;
        this.orderService = orderService;
        this.productOrderRepo = productOrderRepo;


        productEanIdList.add(ProductOrderBootstrap.PRODUCT_EAN_1);
        productEanIdList.add(ProductOrderBootstrap.PRODUCT_EAN_2);
        productEanIdList.add(ProductOrderBootstrap.PRODUCT_EAN_3);
    }

    @Transactional
    @Scheduled(fixedRate = 30000)
    public void placeTastingRoomOrder() {
        List<Customer> customerList = customerRepo.findAllCustomersByCustomerNameLike(ProductOrderBootstrap.CHAOS_MONKEY); //DEBUG this is 0, should be 1

        // there should only be 1 customer (= chaosmonkey)
        if (customerList.size() == 1) {
            doPlaceOrder(customerList.get(0));
        } else {
            log.error("Too many/few tasting room customers found...");
        }
    }

    private void doPlaceOrder(Customer customer) {
        String desiredEanId = getRandomEanId();
 
        ProductOrderLineDto beerOrderLineDto = ProductOrderLineDto.builder()
                .eanId(desiredEanId)
                .orderQuantity(new Random().nextInt(6) +1)
                .build();

        List<ProductOrderLineDto> productOrderLineDtoList = new ArrayList<>();
        productOrderLineDtoList.add(beerOrderLineDto);

        ProductOrderDto beerOrderDto = ProductOrderDto.builder()
                .customerId(customer.getId())
                .customerRef(UUID.randomUUID().toString())
                .productOrderLines(productOrderLineDtoList)
                .build();

        orderService.placeOrder(customer.getId(), beerOrderDto);
    }


    private String getRandomEanId() {
        return productEanIdList.get(
                new Random().nextInt(productEanIdList.size())
        );
    }
}
