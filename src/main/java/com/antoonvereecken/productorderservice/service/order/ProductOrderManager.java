package com.antoonvereecken.productorderservice.service.order;

import com.antoonvereecken.common.model.ProductOrderDto;
import com.antoonvereecken.productorderservice.domain.ProductOrder;

import java.util.UUID;

public interface ProductOrderManager {

    ProductOrder newProductOrder(ProductOrder productOrder);

    void processValidationResult(UUID productId, Boolean isValid);

    void productOrderAllocationSuccess(ProductOrderDto productOrderDto);
    void productOrderAllocationPendingInventory(ProductOrderDto productOrderDto);
    void productOrderAllocationFailed(ProductOrderDto productOrderDto);

    void cancelOrder(UUID orderId);

    void productOrderDelivered(UUID orderId);
}
