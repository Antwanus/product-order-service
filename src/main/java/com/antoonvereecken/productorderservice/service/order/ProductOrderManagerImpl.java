package com.antoonvereecken.productorderservice.service.order;

import com.antoonvereecken.common.model.ProductOrderDto;
import com.antoonvereecken.productorderservice.domain.ProductOrder;
import com.antoonvereecken.productorderservice.domain.ProductOrderEventEnum;
import com.antoonvereecken.productorderservice.domain.ProductOrderStatusEnum;
import com.antoonvereecken.productorderservice.repository.ProductOrderRepo;
import com.antoonvereecken.productorderservice.statemachine.interceptor.OrderStateChangeInterceptor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.statemachine.StateMachine;
import org.springframework.statemachine.config.StateMachineFactory;
import org.springframework.statemachine.support.DefaultStateMachineContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
@RequiredArgsConstructor
@Service
public class ProductOrderManagerImpl implements ProductOrderManager {

    public static final String ORDER_ID_HEADER = "ORDER_ID_HEADER";
    private static final String LOG_PREFIX = "ORDER NOT FOUND!! ID: ";

    private final StateMachineFactory<ProductOrderStatusEnum, ProductOrderEventEnum> smf;
    private final ProductOrderRepo orderRepo;
    private final OrderStateChangeInterceptor orderStateChangeInterceptor;

    @Transactional
    @Override
    public ProductOrder newProductOrder(ProductOrder productOrder) {
        productOrder.setId(null);
        productOrder.setProductOrderStatus(ProductOrderStatusEnum.NEW);
        ProductOrder savedOrder = orderRepo.saveAndFlush(productOrder);

        sendProductOrderEvent(savedOrder, ProductOrderEventEnum.VALIDATE_ORDER);

        return savedOrder;
    }

    @Transactional
    @Override
    public void processValidationResult(UUID productId, Boolean isValid) {
        log.debug("Process Validation Result for beerOrderId: " + productId + " Valid? " + isValid);

        Optional<ProductOrder> productOrderOptional = orderRepo.findById(productId);

        productOrderOptional.ifPresentOrElse(productOrder -> {

            if (Boolean.TRUE.equals(isValid)) {
                sendProductOrderEvent(productOrder, ProductOrderEventEnum.VALIDATION_SUCCESS);

                awaitForStatus(productId, ProductOrderStatusEnum.VALIDATED);

                ProductOrder validatedOrder = orderRepo.findById(productId).get(); // new reference with VALIDATION_SUCCESS
                sendProductOrderEvent(validatedOrder, ProductOrderEventEnum.ALLOCATE_ORDER);

            } else {
                sendProductOrderEvent(productOrder, ProductOrderEventEnum.VALIDATION_ERROR);

            }
        },
                () -> log.error(LOG_PREFIX + productId));

    }

    @Override
    public void productOrderAllocationSuccess(ProductOrderDto productOrderDto) {
        Optional<ProductOrder> productOrderOptional = orderRepo.findById(productOrderDto.getId());

        productOrderOptional.ifPresentOrElse( productOrder -> {
            sendProductOrderEvent(productOrder, ProductOrderEventEnum.ALLOCATION_SUCCESS);

            awaitForStatus(productOrder.getId(), ProductOrderStatusEnum.ALLOCATED);

            updateAllocatedQty(productOrderDto);

        }, () -> log.error(LOG_PREFIX + productOrderDto.getId()));

    }

    @Override
    public void productOrderAllocationPendingInventory(ProductOrderDto productOrderDto) {
        Optional<ProductOrder> productOrderOptional = orderRepo.findById(productOrderDto.getId());

        productOrderOptional.ifPresentOrElse( productOrder -> {
            sendProductOrderEvent(productOrder, ProductOrderEventEnum.ALLOCATION_NO_INVENTORY);

            awaitForStatus(productOrder.getId(), ProductOrderStatusEnum.INVENTORY_PENDING);

            updateAllocatedQty(productOrderDto);

        }, () -> log.error(LOG_PREFIX + productOrderDto.getId()));


    }

    @Override
    public void productOrderAllocationFailed(ProductOrderDto productOrderDto) {
        Optional<ProductOrder> productOrderOptional = orderRepo.findById(productOrderDto.getId());

        productOrderOptional.ifPresentOrElse(productOrder -> {
            sendProductOrderEvent(productOrder, ProductOrderEventEnum.ALLOCATION_ERROR);

        }, () -> log.error(LOG_PREFIX + productOrderDto.getId()));
    }

    @Override
    public void cancelOrder(UUID orderId) {
        orderRepo.findById(orderId).ifPresentOrElse(order -> {
            sendProductOrderEvent(order, ProductOrderEventEnum.ORDER_CANCELED);
        }, () -> log.error(LOG_PREFIX + orderId));
    }

    @Override
    public void productOrderDelivered(UUID id) {
        Optional<ProductOrder> productOrderOptional = orderRepo.findById(id);

        productOrderOptional.ifPresentOrElse(productOrder -> {
            sendProductOrderEvent(productOrder, ProductOrderEventEnum.ORDER_COMPLETED);
        }, () -> log.error(LOG_PREFIX + id));

    }

    /* Update/persist each line of the order in the DB */
    private void updateAllocatedQty(ProductOrderDto productOrderDto) {
        Optional<ProductOrder> allocatedOrderOptional = orderRepo.findById(productOrderDto.getId());

        /* copy quantityAllocated from dto -> entity */
        allocatedOrderOptional.ifPresentOrElse(allocatedOrder -> {
            allocatedOrder.getProductOrderLines().forEach(productOrderLine -> {
                productOrderDto.getProductOrderLines().forEach(productOrderLineDto -> {
                    if(productOrderLine.getId().equals(productOrderLineDto.getId())) {
                        productOrderLine.setAllocatedQuantity(productOrderLineDto.getAllocatedQuantity());
                    }
                });
            });

            orderRepo.saveAndFlush(allocatedOrder);

        }, () -> log.error(LOG_PREFIX + productOrderDto.getId()));

    }

    private void awaitForStatus(UUID orderId, ProductOrderStatusEnum status) {
        AtomicBoolean stopLoop = new AtomicBoolean(false);
        AtomicInteger loopCount = new AtomicInteger(0);

        while ( !stopLoop.get() ) {
            if (loopCount.incrementAndGet() > 10) {
                stopLoop.set(true);
                log.debug("Max retries exceeded (10)");
            }

            orderRepo.findById(orderId).ifPresentOrElse(order -> {
                if(order.getProductOrderStatus().equals(status)) {
                    stopLoop.set(true);
                    log.debug("order found");
                } else {
                    log.debug("orderStatus not found EXPECTED : " + status.name() + " <-> FOUND: " + order.getProductOrderStatus().name());
                }
            }, () -> {
                log.debug("order not found id: " + orderId);
            });

            if( !stopLoop.get() ) {
                try {
                    log.debug("Taking a nap for retry (100 millis)");
                    Thread.sleep(100);
                } catch (Exception e) {
                    // do nothing
                }
            }
        }
    }

    private void sendProductOrderEvent(ProductOrder productOrder, ProductOrderEventEnum event) {
        StateMachine<ProductOrderStatusEnum, ProductOrderEventEnum> sm = buildStateMachineFromProductOrder(productOrder);

        // send msg to SM
        Message<ProductOrderEventEnum> message = MessageBuilder
                .withPayload(event)
                .setHeader(ORDER_ID_HEADER, productOrder.getId().toString())
                .build();

        sm.sendEvent(message);
    }

    private StateMachine<ProductOrderStatusEnum, ProductOrderEventEnum> buildStateMachineFromProductOrder(ProductOrder productOrder) {
        StateMachine<ProductOrderStatusEnum, ProductOrderEventEnum> sm = smf.getStateMachine(productOrder.getId());
        log.debug(sm.toString());

        sm.stop();
        sm.getStateMachineAccessor()
                .doWithAllRegions(stateMachineAccessor -> {
                    stateMachineAccessor.addStateMachineInterceptor(orderStateChangeInterceptor);

                    stateMachineAccessor.resetStateMachine(
                            new DefaultStateMachineContext<>(
                                    productOrder.getProductOrderStatus(),
                                    null,
                                    null,
                                    null
                            ));
                });

        sm.start();

        return sm;
    }


}
