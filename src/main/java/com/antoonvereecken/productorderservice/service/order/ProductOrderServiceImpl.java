package com.antoonvereecken.productorderservice.service.order;

import com.antoonvereecken.common.model.ProductOrderDto;
import com.antoonvereecken.common.model.ProductOrderPagedList;
import com.antoonvereecken.productorderservice.domain.Customer;
import com.antoonvereecken.productorderservice.domain.ProductOrder;
import com.antoonvereecken.productorderservice.domain.ProductOrderStatusEnum;
import com.antoonvereecken.productorderservice.repository.CustomerRepo;
import com.antoonvereecken.productorderservice.repository.ProductOrderRepo;
import com.antoonvereecken.productorderservice.web.mapper.ProductOrderMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
@Service
public class ProductOrderServiceImpl implements ProductOrderService {

    private final CustomerRepo customerRepo;
    private final ProductOrderRepo productOrderRepo;
    private final ProductOrderMapper productOrderMapper;
    private final ProductOrderManager productOrderManager;

    @Override
    public ProductOrderPagedList listOrdersByCustomerId(UUID customerId, Pageable pageable) {
        Optional<Customer> c = customerRepo.findById(customerId);

        if(c.isPresent()) {
            Page<ProductOrder> productOrderPage = productOrderRepo.findAllByCustomer(c.get(), pageable);

            return new ProductOrderPagedList(
                    productOrderPage.stream()
                            .map(productOrderMapper::productOrderToDto)
                            .collect(Collectors.toList()),
                    PageRequest.of(
                            productOrderPage.getPageable().getPageNumber(),
                            productOrderPage.getPageable().getPageSize()),
                            productOrderPage.getTotalElements()
            );
        } else {
            return null;
        }
    }

    @Transactional
    @Override
    public ProductOrderDto placeOrder(UUID customerId, ProductOrderDto orderDto) {
        Optional<Customer> customerOptional = customerRepo.findById(customerId);

        if (customerOptional.isPresent()) {
            ProductOrder productOrder = productOrderMapper.dtoToProductOrder(orderDto);
            productOrder.setId(null); // enforce null to be sure -> hibernate will set this
            productOrder.setCustomer(customerOptional.get()); // set customer to DB record
            productOrder.setProductOrderStatus(ProductOrderStatusEnum.NEW);

            productOrder.getProductOrderLines().forEach(line -> line.setProductOrder(productOrder));

            ProductOrder savedProductOrder = productOrderManager.newProductOrder(productOrder);

            log.debug("SAVED ORDER = " + productOrder.getId());

            return productOrderMapper.productOrderToDto(savedProductOrder);

        }
            throw new RuntimeException("Customer Not found");
    }
    @Override
    public ProductOrderDto getOrderById(UUID customerId, UUID orderId) {
        return productOrderMapper.productOrderToDto(getOrder(customerId, orderId));
    }

    @Override
    public void pickupOrder(UUID customerId, UUID orderId) {
        productOrderManager.productOrderDelivered(orderId);
    }

    private ProductOrder getOrder(UUID customerId, UUID orderId) {
        Optional<Customer> customerOptional = customerRepo.findById(customerId);

        if(customerOptional.isPresent()) {
            Optional<ProductOrder> productOrderOptional = productOrderRepo.findById(orderId);

            if (productOrderOptional.isPresent()) {
                ProductOrder productOrder = productOrderOptional.get();

                if(productOrder.getCustomer().getId().equals(customerId)) {
                    return productOrder;
                }
            }
            throw new RuntimeException("ORDER NOT FOUND");
        }
        throw new RuntimeException("CUSTOMER NOT FOUDN");


    }

}
