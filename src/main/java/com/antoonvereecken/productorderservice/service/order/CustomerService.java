package com.antoonvereecken.productorderservice.service.order;

import com.antoonvereecken.common.model.CustomerPagedList;
import org.springframework.data.domain.Pageable;

public interface CustomerService {

    CustomerPagedList listAllCustomers(Pageable pageable);
}
