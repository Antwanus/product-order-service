package com.antoonvereecken.productorderservice.service.order;

import com.antoonvereecken.common.model.CustomerPagedList;
import com.antoonvereecken.productorderservice.domain.Customer;
import com.antoonvereecken.productorderservice.repository.CustomerRepo;
import com.antoonvereecken.productorderservice.web.mapper.CustomerMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepo customerRepo;
    private final CustomerMapper mapper;

    @Override
    public CustomerPagedList listAllCustomers(Pageable pageable) {

        Page<Customer> customerPage = customerRepo.findAll(pageable);

        return new CustomerPagedList(
                customerPage.stream()
                        .map(mapper::customerToDto)
                        .collect(Collectors.toList()
                ),
                PageRequest.of(
                        customerPage.getPageable().getPageNumber(),
                        customerPage.getPageable().getPageSize()
                ),
                customerPage.getTotalElements()
        );
    }
}
