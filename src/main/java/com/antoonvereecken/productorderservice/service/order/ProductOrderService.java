package com.antoonvereecken.productorderservice.service.order;

import com.antoonvereecken.common.model.ProductOrderDto;
import com.antoonvereecken.common.model.ProductOrderPagedList;
import org.springframework.data.domain.Pageable;

import java.util.UUID;

public interface ProductOrderService {

    ProductOrderPagedList listOrdersByCustomerId(UUID customerId, Pageable pageable);

    ProductOrderDto placeOrder(UUID customerId, ProductOrderDto orderDto);

    ProductOrderDto getOrderById(UUID customerId, UUID orderId);

    void pickupOrder(UUID customerId, UUID orderId);
}
