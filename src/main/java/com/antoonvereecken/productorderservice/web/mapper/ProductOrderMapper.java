package com.antoonvereecken.productorderservice.web.mapper;

import com.antoonvereecken.common.model.ProductOrderDto;
import com.antoonvereecken.productorderservice.domain.ProductOrder;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(uses = { DateMapper.class, ProductOrderLineMapper.class })
public interface ProductOrderMapper {

    @Mapping(target = "customerId", source = "customer.id")
    ProductOrderDto productOrderToDto(ProductOrder productOrder);

    @InheritInverseConfiguration
    ProductOrder dtoToProductOrder(ProductOrderDto dto);


}
