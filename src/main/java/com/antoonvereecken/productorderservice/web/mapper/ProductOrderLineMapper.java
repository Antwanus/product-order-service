package com.antoonvereecken.productorderservice.web.mapper;

import com.antoonvereecken.common.model.ProductOrderLineDto;
import com.antoonvereecken.productorderservice.domain.ProductOrderLine;
import org.mapstruct.DecoratedWith;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@DecoratedWith(ProductOrderLineMapperDecorator.class)
@Mapper(uses = { DateMapper.class })
public interface ProductOrderLineMapper {

     @Mapping(target = "productCategory", ignore = true)
     @Mapping(target = "productName", ignore = true)
     @Mapping(target = "price", ignore = true)
     ProductOrderLineDto productOrderLineToDto(ProductOrderLine productOrderLine);

     @Mapping(target = "productOrder", ignore = true)
     ProductOrderLine dtoToProductOrderLine(ProductOrderLineDto dto);

}
