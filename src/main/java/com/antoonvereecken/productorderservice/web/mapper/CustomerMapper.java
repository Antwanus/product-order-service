package com.antoonvereecken.productorderservice.web.mapper;

import com.antoonvereecken.common.model.CustomerDto;
import com.antoonvereecken.productorderservice.domain.Customer;
import org.mapstruct.Mapper;

@Mapper(uses = { DateMapper.class })
public interface CustomerMapper {

    CustomerDto customerToDto(Customer customer);

    Customer dtoToCustomer(CustomerDto dto);


}
