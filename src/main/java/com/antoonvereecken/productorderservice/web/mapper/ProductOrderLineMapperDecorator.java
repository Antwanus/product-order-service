package com.antoonvereecken.productorderservice.web.mapper;

import com.antoonvereecken.common.model.ProductDto;
import com.antoonvereecken.common.model.ProductOrderLineDto;
import com.antoonvereecken.productorderservice.domain.ProductOrderLine;
import com.antoonvereecken.productorderservice.service.product.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.Optional;

public abstract class ProductOrderLineMapperDecorator implements ProductOrderLineMapper {

    private ProductService productService;
    private ProductOrderLineMapper mapper;

    @Autowired
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }
    @Autowired
    @Qualifier("delegate")
    public void setMapper(ProductOrderLineMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public ProductOrderLineDto productOrderLineToDto(ProductOrderLine orderLine) {
        ProductOrderLineDto orderLineDto = mapper.productOrderLineToDto(orderLine);
        Optional<ProductDto> optionalProductDtoFromDB = productService.getProductByEanId(orderLine.getEanId());

        optionalProductDtoFromDB.ifPresent(e -> {
            orderLineDto.setEanId(e.getEanId());
            orderLineDto.setProductId(e.getId());
            orderLineDto.setProductName(e.getProductName());
            orderLineDto.setProductCategory(e.getProductCategory());
            orderLineDto.setPrice(e.getProductPrice());
        });

        return orderLineDto;
    }

}
