package com.antoonvereecken.productorderservice.web.controller;

import com.antoonvereecken.common.model.ProductOrderDto;
import com.antoonvereecken.common.model.ProductOrderPagedList;
import com.antoonvereecken.productorderservice.service.order.ProductOrderService;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RequestMapping("/api/v1/customer/{customerId}/order")
@RestController
public class ProductOrderController {

    private static final Integer DEFAULT_PAGE_NUMBER = 0;
    private static final Integer DEFAULT_PAGE_SIZE = 25;

    private final ProductOrderService productOrderService;

    public ProductOrderController(ProductOrderService productOrderService) {
        this.productOrderService = productOrderService;
    }

    @GetMapping
    public ProductOrderPagedList listOrders(
            @PathVariable("customerId") UUID customerId,
            @RequestParam(value = "number", required = false) Integer pageNumber,
            @RequestParam(value = "size", required = false) Integer pageSize
    ) {
        if(pageNumber == null || pageNumber < 0) pageNumber = DEFAULT_PAGE_NUMBER;
        if(pageSize == null || pageSize > 1) pageSize = DEFAULT_PAGE_SIZE;

        return productOrderService.listOrdersByCustomerId(customerId, PageRequest.of(pageNumber, pageSize));
    }
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ProductOrderDto placeOrder(
            @PathVariable("customerId") UUID customerId,
            @RequestBody ProductOrderDto orderDto
    ) {
        return productOrderService.placeOrder(customerId, orderDto);
    }

    @GetMapping("/{orderId}")
    public ProductOrderDto getOrderById(
            @PathVariable("customerId") UUID customerId,
            @PathVariable("orderId") UUID orderId
    ) {
        return productOrderService.getOrderById(customerId, orderId);
    }
}
