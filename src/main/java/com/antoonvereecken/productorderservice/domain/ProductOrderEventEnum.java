package com.antoonvereecken.productorderservice.domain;

public enum ProductOrderEventEnum {

    VALIDATE_ORDER,
        VALIDATION_SUCCESS, VALIDATION_ERROR,

    ALLOCATE_ORDER,
        ALLOCATION_SUCCESS, ALLOCATION_NO_INVENTORY, ALLOCATION_ERROR,

    ORDER_CANCELED,

    ORDER_COMPLETED

}
