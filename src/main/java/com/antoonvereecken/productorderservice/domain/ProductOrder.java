package com.antoonvereecken.productorderservice.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class ProductOrder extends BaseEntity{

    private String customerRef;

    @Enumerated(EnumType.STRING)
    @Column(length = 20, columnDefinition = "varchar(20)")
    private ProductOrderStatusEnum productOrderStatus = ProductOrderStatusEnum.NEW;

    private String orderStatusCallbackUrl;

    @ManyToOne
    private Customer customer;

    @OneToMany(mappedBy = "productOrder", cascade = CascadeType.ALL)
    @Fetch(FetchMode.JOIN)
    private Set<ProductOrderLine> productOrderLines;

    @Builder
    public ProductOrder(
            UUID id,
            Long version,
            Timestamp createdDate,
            Timestamp lastModifiedDate,
            String customerRef,
            ProductOrderStatusEnum productOrderStatus,
            String orderStatusCallbackUrl,
            Customer customer,
            Set<ProductOrderLine> productOrderLines
    ) {
        super(id, version, createdDate, lastModifiedDate);
        this.customerRef = customerRef;
        this.productOrderStatus = productOrderStatus;
        this.orderStatusCallbackUrl = orderStatusCallbackUrl;
        this.customer = customer;
        this.productOrderLines = productOrderLines;
    }
}
