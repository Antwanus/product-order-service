package com.antoonvereecken.productorderservice.domain;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import java.sql.Timestamp;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@Entity
public class ProductOrderLine extends BaseEntity {

    @ManyToOne
    private ProductOrder productOrder;

    private UUID productId;

    @Column(length = 13, columnDefinition = "varchar(13)")
    private String eanId;

    private Integer orderQuantity = 0;

    private Integer allocatedQuantity = 0;

    @Builder
    public ProductOrderLine(UUID id, Long version, Timestamp createdDate, Timestamp lastModifiedDate, ProductOrder productOrder, UUID productId, String eanId, Integer orderQuantity, Integer allocatedQuantity) {
        super(id, version, createdDate, lastModifiedDate);
        this.productOrder = productOrder;
        this.productId = productId;
        this.eanId = eanId;
        this.orderQuantity = orderQuantity;
        this.allocatedQuantity = allocatedQuantity;
    }
}
